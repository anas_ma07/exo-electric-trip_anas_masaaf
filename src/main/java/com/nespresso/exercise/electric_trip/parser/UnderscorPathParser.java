package com.nespresso.exercise.electric_trip.parser;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.exercise.electric_trip.City;

public class UnderscorPathParser implements PathParser {

	private static final String SEPARATOR = "-";

	// PARIS-200-BOURGES
	public Map<String, City> parse(String pathDescription) {

		Map<String, City> cityParsed = new HashMap<String, City>();

		String[] pathDescriptionSplited = pathDescription.split(SEPARATOR);

		String departureCityName = "";
		String destinationCityName = "";
		String distanceText = "";
		City destinationCity;
		City departureCity;

		for (int i = 0; i < pathDescriptionSplited.length-2; i=i+2) {
			departureCityName = pathDescriptionSplited[i].trim();
			destinationCityName = pathDescriptionSplited[i + 2].trim();
			distanceText = pathDescriptionSplited[i + 1].trim();
			int distance = Integer.valueOf(distanceText);
			destinationCity = new City(destinationCityName, null, 0);
			departureCity = new City(departureCityName, destinationCity,distance);
			cityParsed.put(departureCityName, departureCity);
		}

		return cityParsed;
	}

}
