package com.nespresso.exercise.electric_trip.parser;

import java.util.Map;

import com.nespresso.exercise.electric_trip.City;

public interface PathParser {

	public Map<String, City> parse(String pathDescription);

}
