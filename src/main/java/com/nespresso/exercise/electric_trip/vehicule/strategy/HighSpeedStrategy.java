package com.nespresso.exercise.electric_trip.vehicule.strategy;


public class HighSpeedStrategy extends SpeedStrategy {

	@Override
	public int maxCanDrive(int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
		
		return batterySize * highSpeedPerformance;
	}

}
