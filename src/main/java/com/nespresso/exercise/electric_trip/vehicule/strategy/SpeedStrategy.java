package com.nespresso.exercise.electric_trip.vehicule.strategy;

public abstract class SpeedStrategy {

	public abstract int maxCanDrive(int batterySize, int lowSpeedPerformance, int highSpeedPerformance);

		
}
