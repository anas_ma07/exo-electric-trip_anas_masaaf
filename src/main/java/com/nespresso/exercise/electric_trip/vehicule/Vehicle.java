package com.nespresso.exercise.electric_trip.vehicule;

import com.nespresso.exercise.electric_trip.vehicule.strategy.SpeedStrategy;

public class Vehicle {


	private int batterySize;
	private int lowSpeedPerformance;
	private int highSpeedPerformance;
	private SpeedStrategy speedStrategy;

	public Vehicle(int batterySize, int lowSpeedPerformance,
			int highSpeedPerformance) {
		super();
		this.batterySize = batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
	}

	public void speedStartegy(SpeedStrategy strategy) {
		this.speedStrategy = strategy;
	}

	public int maxCanDrive() {
		return speedStrategy.maxCanDrive(batterySize, lowSpeedPerformance, highSpeedPerformance);
	}

	
}
