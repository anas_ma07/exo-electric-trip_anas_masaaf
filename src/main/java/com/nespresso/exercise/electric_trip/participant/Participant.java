package com.nespresso.exercise.electric_trip.participant;

import java.util.Map;
import java.util.Map.Entry;

import com.nespresso.exercise.electric_trip.City;
import com.nespresso.exercise.electric_trip.vehicule.Vehicle;
import com.nespresso.exercise.electric_trip.vehicule.strategy.LowSpeedStrategy;

public class Participant {

	private City departureCity;
	private Vehicle vehicle;

	public Participant(City departureCity, Vehicle vehicle) {
		super();
		this.departureCity = departureCity;
		this.vehicle = vehicle;
	}

	public void lowSpeed() {
		this.vehicle.speedStartegy(new LowSpeedStrategy());
	}

	public String location(Map<String, City> cities) {
		int maxVehicleCanDrive = this.vehicle.maxCanDrive();
		int distancePassed = 0;

		String nextLocation = "";
		String cityName;
		for (Entry<String, City> city : cities.entrySet()) {

			distancePassed = this.departureCity.distancePassed(distancePassed);
			nextLocation = this.departureCity.nextLocation(maxVehicleCanDrive, distancePassed);
			
			cityName = city.getKey();

			if (nextLocation.equals(cityName))
				return nextLocation;
		}

		return nextLocation;
	}

	public int chargeOf() {
		int maxVehicleCanDrive = this.vehicle.maxCanDrive();
		return this.departureCity.chargeOf(maxVehicleCanDrive);
	}

}
