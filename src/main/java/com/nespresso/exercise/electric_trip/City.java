package com.nespresso.exercise.electric_trip;

public class City {

	private String name;
	private City destinationCity;
	private int distance;

	public City(String name, City destinationCity, int distance) {
		super();
		this.name = name;
		this.destinationCity = destinationCity;
		this.distance = distance;
	}

	
	public String nextLocation(int maxVehicleCanDrive , int distancePassed) {
		if (maxVehicleCanDrive >= distancePassed)
			return destinationCity.name;
		else
			return this.name;
	}
	
	public int chargeOf(int maxVehicleCanDrive) {
		double chargeOn = (double) this.distance / maxVehicleCanDrive;
		double chargeOf = 1- chargeOn;
		int chargeOfPercent = (int) Math.round(chargeOf*100);
		
		return chargeOfPercent;
	}
	
	public int distancePassed(int distancePassed) {
		return distancePassed + this.distance;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((name == null) ? 0 : name
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (name != other.name)
			return false;
		return true;
	}

}
