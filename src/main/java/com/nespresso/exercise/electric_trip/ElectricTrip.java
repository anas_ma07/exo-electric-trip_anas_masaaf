package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nespresso.exercise.electric_trip.parser.PathParser;
import com.nespresso.exercise.electric_trip.parser.UnderscorPathParser;
import com.nespresso.exercise.electric_trip.participant.Participant;
import com.nespresso.exercise.electric_trip.reporter.ChargePercentReporter;
import com.nespresso.exercise.electric_trip.reporter.ChargeReporter;
import com.nespresso.exercise.electric_trip.vehicule.Vehicle;

public class ElectricTrip {

	private Map<String, City> cities;
	private List<Participant> participants;

	public ElectricTrip(String pathDescription) {
		this.cities = new HashMap<String, City>();
		this.participants = new ArrayList<Participant>();
		PathParser parser = new UnderscorPathParser();
		this.cities.putAll(parser.parse(pathDescription));
	}

	public int startTripIn(String departureCityName, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
		City departureCity = this.cities.get(departureCityName);
		Vehicle vehicle = new Vehicle(batterySize, lowSpeedPerformance, highSpeedPerformance);
		Participant participant = new Participant(departureCity, vehicle);
		this.participants.add(participant);
		int idParticipant = this.participants.size()-1;
		
		return idParticipant;
	}

	public void go(int participantId) {
		Participant participant = this.participants.get(participantId);
		participant.lowSpeed();

	}

	public String locationOf(int participantId) {
		Participant participant = this.participants.get(participantId);
		
		return participant.location(cities);
	}

	public String chargeOf(int participantId) {
		Participant participant = this.participants.get(participantId);
		ChargeReporter reporter = new ChargePercentReporter();
		int chargeOf = participant.chargeOf();
		return reporter.report(chargeOf);
	}

	public void sprint(int participantId) {
		
		Participant participant = this.participants.get(participantId);

	}

}
