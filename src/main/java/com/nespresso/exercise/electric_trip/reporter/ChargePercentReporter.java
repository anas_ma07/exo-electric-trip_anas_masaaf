package com.nespresso.exercise.electric_trip.reporter;

public class ChargePercentReporter implements ChargeReporter {

	private static final String PERCENT= "%";
	public String report(int charge){
		StringBuilder builder = new StringBuilder(String.valueOf(charge));
		builder.append(PERCENT);
		return builder.toString();
	}
}
