package com.nespresso.exercise.electric_trip.reporter;

public interface ChargeReporter {

	public String report(int charge);
}
